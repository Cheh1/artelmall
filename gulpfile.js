'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const gulpIf = require('gulp-if');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const pug = require('gulp-pug');
const browserSync = require('browser-sync').create();

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';
// to production run (не включает sourcemap в конечный файл при сборке): NODE_ENV=production gulp 
const reload = browserSync.reload;

// Compile sass files to css
gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
      .pipe(gulpIf(isDevelopment, sourcemaps.init()))
//      .pipe(cleanCSS())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
        browser: ['last 2 versions', 'IE 9'],
        cascade: false
      }))
      .pipe(gulpIf(isDevelopment, sourcemaps.write()))
      .pipe(gulp.dest('./site/css'))
      .pipe(cleanCSS())
      .pipe(rename("style.min.css"))
      .pipe(gulp.dest('./site/css/'))
      .pipe(browserSync.reload({stream:true}));
});

//gulp.task('minify-css', function() {
//  return gulp.src('./site/css/style.css')
//    .pipe(cleanCSS())
//    .pipe(rename("style.min.css"))
//    .pipe(gulp.dest('./site/css/'));
//});

// Compile pug files to html
gulp.task('pug', () =>{
  return gulp.src('./_pugfiles/**/*.pug')
    .pipe(pug(
        {pretty: "\t"}
    ))
    .pipe(gulp.dest('./site'));
});

// the working directory
gulp.task('browser-sync',[
    'sass',
//    'minify-css',
    'pug'
] ,function() {
    browserSync.init({
        server: {
            baseDir: "./site"
        }
    });
});

// Watch files comiling
gulp.task('watch', function () {
  gulp.watch('./scss/*.scss', ['sass']);
  gulp.watch('./_pugfiles/**/*.pug', ['pug']);
  gulp.watch('./site/*.html').on('change', reload);
});


gulp.task('default', ['watch', 'browser-sync']);
